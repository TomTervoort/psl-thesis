Test finished: StepLimitreached
Message coverage: 67.6%
BadResponse: 0.0%
BadResponse
-
. StatusResponse
  -
  . N
  . -
    . StatusResponseId
      -
      . N
      . -
        . N
        . -
          . N
          . -
            . N
            . N
    . -
      . N
      . N
. N
Bye: 0.0%
Bye
-
. UntaggedStatusResponse
  -
  . N
  . -
    . StatusResponseId
      -
      . N
      . -
        . N
        . -
          . N
          . -
            . N
            . N
    . -
      . N
      . N
. N
CloseCommand: 100.0%
CloseCommand
-
. SimpleCommand
  -
  . Y
  . -
    . Y
    . Y
. Y
CopyCommand: 100.0%
CopyCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . SequenceSet
    -
    . Y
    . -
      . Y
      . Y
  . -
    . Y
    . Y
CreateCommand: 100.0%
CreateCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . Y
  . Y
DeleteCommand: 0.0%
DeleteCommand
-
. CommandStart
  -
  . N
  . -
    . N
    . N
. -
  . N
  . N
ExamineCommand: 100.0%
ExamineCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . Y
  . Y
ExamineResponse: 100.0%
ExamineResponse
-
. Y
. -
  . Y
  . -
    . Y
    . Y
FetchCommand: 75.0%
FetchCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . SequenceSet
    -
    . Y
    . -
      . Y
      . Y
  . -
    . MessageDataItems
      -
      . N
      . -
        . N
        . Y
    . Y
FetchResponse: 100.0%
FetchResponse
-
. UntaggedResponse
  -
  . Y
  . -
    . Y
    . -
      . Y
      . -
        . Y
        . Y
. Y
FlagsResponse: 100.0%
FlagsResponse
-
. Y
. -
  . Y
  . -
    . Y
    . Y
LoginCommand: 100.0%
LoginCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . Y
  . -
    . Y
    . Y
LogoutCommand: 0.0%
LogoutCommand
-
. SimpleCommand
  -
  . N
  . -
    . N
    . N
. N
NoResponse: 83.33%
NoResponse
-
. StatusResponse
  -
  . Y
  . -
    . StatusResponseId
      -
      . Y
      . -
        . Y
        . -
          . N
          . -
            . N
            . N
    . -
      . Y
      . Y
. Y
OkResponse: 83.33%
OkResponse
-
. StatusResponse
  -
  . Y
  . -
    . StatusResponseId
      -
      . Y
      . -
        . Y
        . -
          . N
          . -
            . N
            . N
    . -
      . Y
      . Y
. Y
PreAuthGreeting: 0.0%
PreAuthGreeting
-
. UntaggedStatusResponse
  -
  . N
  . -
    . StatusResponseId
      -
      . N
      . -
        . N
        . -
          . N
          . -
            . N
            . N
    . -
      . N
      . N
. N
RenameCommand: 100.0%
RenameCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . Y
  . -
    . Y
    . Y
SelectCommand: 100.0%
SelectCommand
-
. CommandStart
  -
  . Y
  . -
    . Y
    . Y
. -
  . Y
  . Y
StoreCommand: 0.0%
StoreCommand
-
. CommandStart
  -
  . N
  . -
    . N
    . N
. -
  . SequenceSet
    -
    . N
    . -
      . N
      . N
  . -
    . FlagOption
      -
      . N
      . -
        . N
        . -
          . N
          . -
            . N
            . -
              . N
              . N
    . -
      . N
      . -
        . N
        . N
UntaggedNo: 83.33%
UntaggedNo
-
. UntaggedStatusResponse
  -
  . Y
  . -
    . StatusResponseId
      -
      . Y
      . -
        . Y
        . -
          . N
          . -
            . N
            . N
    . -
      . Y
      . Y
. Y
UntaggedOk: 83.33%
UntaggedOk
-
. UntaggedStatusResponse
  -
  . Y
  . -
    . StatusResponseId
      -
      . Y
      . -
        . Y
        . -
          . N
          . -
            . N
            . N
    . -
      . Y
      . Y
. Y

State transition coverage: 72.58%
Y (Normal "Authenticated",Receive "CreateCommand",Intermediate 10)
Y (Normal "Authenticated",Receive "CreateCommand",Intermediate 11)
Y (Normal "Authenticated",Receive "ExamineCommand",Normal "Examining")
Y (Normal "Authenticated",Receive "ExamineCommand",Intermediate 9)
Y (Normal "Authenticated",Receive "RenameCommand",Intermediate 12)
N (Normal "Authenticated",Receive "RenameCommand",Intermediate 13)
Y (Normal "Authenticated",Receive "SelectCommand",Normal "Selecting")
Y (Normal "Authenticated",Receive "SelectCommand",Intermediate 8)
N (Normal "Authenticated",Internal,Intermediate 7)
Y (Normal "Examining",Internal,Intermediate 14)
Y (Normal "Examining",Internal,Intermediate 15)
Y (Normal "Examining",Internal,Intermediate 16)
Y (Normal "Examining",Internal,Intermediate 17)
Y (Normal "NotAuthenticated",Receive "LoginCommand",Intermediate 4)
N (Normal "NotAuthenticated",Receive "LoginCommand",Intermediate 6)
N (Normal "NotAuthenticated",Internal,Intermediate 3)
Y (Normal "Processing",Internal,Intermediate 25)
Y (Normal "Processing",Internal,Intermediate 26)
Y (Normal "Processing",Internal,Intermediate 27)
Y (Normal "Processing",Internal,Intermediate 28)
Y (Normal "Selected",Receive "CloseCommand",Intermediate 24)
Y (Normal "Selected",Receive "CopyCommand",Normal "Processing")
Y (Normal "Selected",Receive "FetchCommand",Normal "Processing")
N (Normal "Selected",Receive "StoreCommand",Normal "Processing")
N (Normal "Selected",Internal,Intermediate 22)
N (Normal "Selected",Internal,Intermediate 23)
Y (Normal "Selecting",Internal,Intermediate 18)
Y (Normal "Selecting",Internal,Intermediate 19)
Y (Normal "Selecting",Internal,Intermediate 20)
Y (Normal "Selecting",Internal,Intermediate 21)
Y (Normal "ServerGreeting",Internal,Intermediate 0)
N (Normal "ServerGreeting",Internal,Intermediate 1)
N (Normal "ServerGreeting",Internal,Intermediate 2)
Y (Intermediate 0,Send "UntaggedOk",Normal "NotAuthenticated")
N (Intermediate 1,Send "PreAuthGreeting",Normal "Authenticated")
N (Intermediate 2,Send "Bye",Exit)
N (Intermediate 3,Send "UntaggedOk",Normal "NotAuthenticated")
Y (Intermediate 4,Send "UntaggedOk",Intermediate 5)
Y (Intermediate 5,Send "OkResponse",Normal "Authenticated")
N (Intermediate 6,Send "NoResponse",Normal "NotAuthenticated")
N (Intermediate 7,Send "UntaggedOk",Normal "Authenticated")
Y (Intermediate 8,Send "NoResponse",Normal "Authenticated")
Y (Intermediate 9,Send "NoResponse",Normal "Authenticated")
Y (Intermediate 10,Send "OkResponse",Normal "Authenticated")
Y (Intermediate 11,Send "NoResponse",Normal "Authenticated")
Y (Intermediate 12,Send "OkResponse",Normal "Authenticated")
N (Intermediate 13,Send "NoResponse",Normal "Authenticated")
Y (Intermediate 14,Send "FlagsResponse",Normal "Examining")
Y (Intermediate 15,Send "ExamineResponse",Normal "Examining")
Y (Intermediate 16,Send "UntaggedOk",Normal "Examining")
Y (Intermediate 17,Send "OkResponse",Normal "Authenticated")
Y (Intermediate 18,Send "FlagsResponse",Normal "Selecting")
Y (Intermediate 19,Send "ExamineResponse",Normal "Selecting")
Y (Intermediate 20,Send "UntaggedOk",Normal "Selecting")
Y (Intermediate 21,Send "OkResponse",Normal "Selected")
N (Intermediate 22,Send "UntaggedOk",Normal "Selected")
N (Intermediate 23,Send "UntaggedNo",Normal "Selected")
Y (Intermediate 24,Send "OkResponse",Normal "Authenticated")
Y (Intermediate 25,Send "FetchResponse",Normal "Processing")
Y (Intermediate 26,Send "UntaggedNo",Normal "Processing")
Y (Intermediate 27,Send "OkResponse",Normal "Selected")
Y (Intermediate 28,Send "NoResponse",Normal "Selected")
