APSL: A Protocol Specification Language
=======================================

This repository contains the code and documentation for my thesis project, which I have given the working title APSL 
(A Protocol Specification Language; I will change it once I have come up with a better name). 

This language is presented and described in `my thesis <paper/thesis.pdf>`_.


Core ideas
----------

APSL is intended to be a formal protocol specification language: specifications written in it can be compiled and used 
to aid programmers writing implementations of the protocol in question, primarily by running automated *black-box* 
tests against them. The test program uses the specification in order to be able to combine fuzz testing and state graph 
traversal techniques in order to try to cover as many situations and edge-cases as possible.

APSL really consist of two sub-languages with similar syntax, which I uncreatively dubbed AMSL (A Message Specification
Language) and AISL (An Interaction Specification Language):

An AMSL specification describes the format of the types of messages that can be send as part of the protocol; it tries
to seperate the *semantics* of a message (e.g. "this message contains a text string containing an e-mail address and 
an integer representing the number of messages sent towards it") from the binary *encoding* thereof (e.g. "the address 
is ASCII-encoded and the message count uses an unsigned 32-bit big endian representation") when the message is send 
over the wire.

An AISL specification depends on one or more AMSL modules and describes how the messsages defined therein are supposed 
to be exchanged: actors within a protocol are represented as communicating state machines that can send or receive 
particular messages in particular states. The protocol tester could use this specification in order to *simulate* one 
or more actors and then communicate with one or more actual systems that are to be tested. The program should test for 
correct responses to both correct and incorrect messages in as many states of the protocol as possible.

Besides automated testing, AMSL and AISL definitions could also be used for other purposes that aid protocol 
implementors, such as code generation and data visualization.


File structure
--------------

Documentation
~~~~~~~~~~~~~

The `doc <doc/>`_ directory contains various documentation files, currently only about the AMSL language:

- `amsl-language-definition.rst <doc/amsl-language-definition.rst>`_ describes the structures of the language and how 
  they work.
- `basic-types-codecs.rst <doc/basic-types-codecs.rst>`_ is a reference of the built-in *types* and *codecs* that can 
  be used within AMSL.
- `amsl-grammar.tex <doc/amsl-grammar.tex>`_ contains an automatically generated LaTeX document (from 
  `this source <compiler/src/Language/APSL/AMSL/Grammar.cf>`_ written in the BNFC language) that describes the AMSL 
  language grammar.
- Likewise, `aisl-grammar.tex <doc/amsl-grammar.tex>`_ describes the grammar of the AISL language, also derived from 
  a `BNFC specification <compiler/src/Language/APSL/AISL/Grammar.cf>`_.


The code
~~~~~~~~

The `compiler <compiler/>`_ directory contains a Haskell project with the source code of the APSL compiler, which can serve as a 
library that can parse AMSL and APSL specifications and perform operations with them. Once it works, another project 
will be added that uses this compiler in order to perform automated testing. Refer to 
`doc/compiler-architecture.rst <doc/compiler-architecture.rst>`_ for a description of how it is structured.


Other stuff
~~~~~~~~~~~

- The `examples <examples/>`_ directory contains a few example specifications written in the AMSL and APSL languages.
- The shell script `compiler/src/amsl-bnfc.sh <compiler/src/amsl-bnfc.sh>`_ compiles the BNFC grammar(s). It should 
  eventually be incorporated into an automatic build system such as Cabal or Make.
- The `paper <paper/>`_ directory contains LaTeX files for my thesis and presentation.
- The `test-cases <test-cases/>`_ directory contains specifications used for the case studies in the thesis, along 
  with test results.
