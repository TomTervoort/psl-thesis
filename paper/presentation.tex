\documentclass{beamer}

\mode<presentation>
{
  \usetheme{Madrid}
  \usecolortheme{beaver}
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{graphicx}
\lstset
{
    language=[LaTeX]TeX,
    breaklines=true,
    basicstyle=\tt\scriptsize,
    %commentstyle=\color{green}
    keywordstyle=\color{blue},
    %stringstyle=\color{black}
    identifierstyle=\color{magenta},
}
\graphicspath{{images/}}

\title[Thesis Defense]{A Protocol Specification Language for Aiding and Testing Implementations}
\author{Tom Tervoort}
\date{}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}


\section{The Problem}

%3

\begin{frame}{Communication Protocols}
	\begin{itemize}	
	\item A protocol is a pre-agreed upon \emph{language} systems use to communicate.
	\end{itemize}
	\bigskip
	\centerline{\includegraphics[scale=0.45]{communication-protocol}}
\end{frame}

\begin{frame}{How to Implement a Protocol}
    \begin{itemize}
        \item Read the \alert{specification}.
        \item Identify \alert{messages} and write \emph{parsers} and \emph{serializers} for them.
        \item Program protocol \alert{logic}: trigger correct behaviour on message reception,
              depending on the circumstances.
        \item Test...
    \end{itemize}
\end{frame}

\begin{frame}{Testing is Hard}
    \begin{itemize}
        \item You may not have control over communication peers.
        \item Others may interpret the specification differently.
        \item Performing unit tests requires a protocol \alert{simulator}.
        \item The system needs to deal with invalid messages.
        \item Protection is needed against malicious \alert{attackers}, that seek out obscure 
              corner-cases and try to exploit bugs.    
        \bigskip
        \item It would be nice to automate this!
    \end{itemize}
\end{frame}


\section{Protocol Specification Languages}

%7

\begin{frame}{Specifications for Humans}
	\centerline{\includegraphics[scale=0.5]{images/rfc-example}}
    
    \begin{itemize}
        \item English-language technical documents.
        \item Possibly \alert{ambiguous} or \alert{unclear}.
    \end{itemize}
\end{frame}

\begin{frame}{Specifications for Computers}
    Applications of specifications that are also ``computer-readable'': \bigskip
    
    \begin{itemize}
        \item Enforced unambiguity
        \item Automatic code generation
        \item Traffic visualisation
        \item Automated testing
    \end{itemize}
\end{frame}

\begin{frame}{Message Description Languages}
	\centerline{\includegraphics[scale=0.8]{images/asn1-editor}}
\end{frame}

\begin{frame}{Message Description Languages}
    \begin{itemize}
        \item Options for \emph{text-based} and \emph{binary} protocols.
        \item Here, we will focus on the latter.
        \item Examples of binary message description languages:
        \begin{itemize}
            \item ASN.1            
            \item Google Protocol Buffers
            \item Apache Thrift
        \end{itemize}
        \item These can be used to design new protocols, \alert{but can not describe pre-existing
              ones}.
        \item To describe existing binary protocols, one needs to express a variety of 
              conventions for ordering and formatting messages.
    \end{itemize}
\end{frame}

\begin{frame}{Interaction Description Languages}
    How should a system respond to a message, depending on its state?    
    
    \begin{itemize}
        \item This requires a higher-level specification language.
        \item Various mathematical models of protocols exist, along with languages in which to 
              describe desired properties.
        \item Many verification tools based on them are available.
        \item However, these are aimed at protocol \emph{designers} instead of 
              \emph{implementers}, and verify models instead of real systems.
    \end{itemize}
\end{frame}


\section{Conformance Testing}

%7

\begin{frame}{Conformance Testing}
    \begin{itemize}
        \item Verify whether an implementation matches a specification.
        \item Does not prove anything about the specification, which is assumed to be correct.
        \item Preferably \emph{black-box testing}: the tester can only send messages and observe 
              responses.
    \end{itemize}
    \bigskip
    \centerline{\includegraphics[scale=0.6]{black-box}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item A model for representing protocol interactions.
        \item Allows a tester to determine the state a system is supposed to be in based on observed 
              \alert{actions}.
        \begin{itemize}
            \item An action can be a message received from the tested system.
            \item But it can also be \alert{triggered} by sending one.
            \item Can be used to define a \alert{strategy} about which messages to send.
        \end{itemize}
        \bigskip
        \item An \emph{LTS} is defined as:
        \begin{itemize}
            \item A set of \alert{states} $S$
            \item A set of \alert{observable actions} $L$
            \item A set of \alert{transitions} 
                  $T \subseteq (S \times (L \cup \{\tau\}) \times S)$
            \item An \alert{initial state} $s_{0} \in S$
        \end{itemize}
        \item $\tau$ is an unobservable \alert{internal action}.
    \end{itemize}
\end{frame}

\begin{frame}{Labelled Transition Systems}
        \centerline{\includegraphics[scale=0.2]{lts-animation/lts1}}
        \begin{itemize}
            \item $S = \{A,B,C,D,E\}$
            \item $L = \{1,2,3,4\}$
            \item $T = \{(A,1,B),(A,3,C),(B,2,D),(B,2,C),(D,3,D),(D,4,E),(E,\tau,C)\}$
            \item $s_{0} = A$
        \end{itemize}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{\alert{1}}
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts2}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{1, \alert{2}}
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts3}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{1, 2, \alert{3}}
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts4}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{1, 2, 3, \alert{4}}
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts5}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{1, 2, 3, \alert{4}}
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts6}}
\end{frame}

\begin{frame}{Labelled Transition Systems}
    \begin{itemize}
        \item Observed: \textbf{1, 2, 3, 4, \alert{3}}
        \item No possible state: an \alert{error} has been detected!
    \end{itemize}
    \centerline{\includegraphics[scale=0.2]{lts-animation/lts7}}
\end{frame}


\section{The APSL Language}

%14

\begin{frame}{The APSL Language(s)}
    \begin{itemize}
        \item A formal, human- and computer-readable, protocol specification language.
        \item It should be relatively easy to translate an official specification into APSL.
        \item Many possible applications; the primary focus is \alert{conformance test 
              derivation}.
        \bigskip
        \item Actually split into two languages, that together form APSL:
        \begin{itemize}
            \item AMSL: for describing \alert{messages}
            \item AISL: for describing expected \alert{behaviour}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{AMSL: Fields}
    \centerline{\includegraphics[scale=0.5]{amsl-field}}
    \begin{itemize}
        \item Each \emph{field} has a \alert{type} and a \alert{codec}.
        \begin{itemize}
            \item Codec: method of converting between a type and a binary representation of it.
        \end{itemize}
        \item Uses a kind of \alert{refinement types}: a predicate can be provided in order to 
              restrict the possible values of a field.
    \end{itemize}
\end{frame}

\begin{frame}{AMSL: Records and Messages}
    \centerline{\includegraphics[scale=0.35]{amsl-record}}
    \begin{itemize}
        \item \alert{Records} compose fields and can encode their \alert{relations} to each other.
        \item A record can be marked as being a \alert{message}, allowing it to be send or 
              received by the tester.
    \end{itemize}
\end{frame}

\begin{frame}{AMSL: Unions}
    \centerline{\includegraphics[scale=0.5]{amsl-union}}
    \begin{itemize}
        \item \alert{Unions} are sum types.
        \item Their value depends on a \alert{tag}, which may be encoded along with the union, or
              be derived from the context (i.e. through a field parameter).
    \end{itemize}
\end{frame}

\begin{frame}{Message Specification: Additional Features}
    \begin{itemize}
        \item \emph{Enumeration types} can be defined that can contain one of a small set of 
              values.
        \item Users may define aliases for frequently used types or codecs.
        \item Codecs can be \emph{stacked}, chaining their encode and decode operations.
        \item Types from one AMSL file can be imported in another.
        \item Syntax for defining extension types and codecs.
    \end{itemize}
\end{frame}

\begin{frame}{AISL}
    \begin{itemize}
        \item Uses messages defined in an AMSL module.
        \item Protocol is a set of \alert{actors}, each of which have a number of \alert{states}.
        \item Per state, one or more \alert{reactions} can be defined for each type of incoming 
              message; which reaction is supposed to occur is treated as 
              \alert{non-deterministic}.
        \item A reaction consists of \alert{sending} zero or more responses and possibly moving 
              to another state.
        \bigskip
        \item Describes a model equivalent to a \alert{labelled transition system}.
    \end{itemize}
\end{frame}

\begin{frame}{AISL Example}
    \centerline{\includegraphics[scale=0.35]{coffee-example}}
\end{frame}


\begin{frame}{AISL Example: LTS Derivation}
    \centerline{\includegraphics[scale=0.2]{coffee-lts}}
\end{frame}


\section{The Testing Framework}

%7

\begin{frame}{The APSL Compiler}
    \begin{itemize}
        \item The compiler parses AMSL and AISL modules and transforms them into a Haskell 
              representation.
        \item Each AMSL type is associated to a Haskell type, allowing Haskell functions to be 
              applied to intercepted or generated protocol messages.
        \item Operations can be written to act on AMSL messages or AISL actors. The compiler 
              comes with:
        \begin{itemize}
            \item A message parser and serializer
            \item A message pretty-printer
            \item A message generator for testing
            \item A test runner for an actor
            \item A \emph{coverage measurer}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Message Generation}
    \begin{itemize}
        \item When the tester requires a message to be sent, an instance of it is generated.
        \item A \alert{random testing strategy} is used.
        \item The generator only considers (refined) types; it does not care about codecs.
        \item The generator to use depends on the type.
        \begin{itemize}
            \item Record values are obtained by generating their fields one by one.
            \item Basic types are usually picked by \alert{uniform selection}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{LTS Traversal}
    \begin{itemize}
        \item A simple ``greedy random walk'' strategy is employed.
        \item When deciding what message to send, one is picked that will immediately cause a 
              transfer to a yet unvisited state.
        \item When all neighbouring states are visited, a random one is picked instead.
        \item Repeat this a predefined amount of times.
    \end{itemize}
\end{frame}


\section{Testing the Tester}

%6

\begin{frame}{Test Coverage}
    \begin{itemize}
        \item In order to be able to compare testing strategies, a method is needed to determine
              how well they work.
        \item Idea: look at the APSL specification and measure how much of it is \emph{covered}
              by the tester.
        \bigskip
        \item For AMSL: count the number of \alert{fields} that are encountered in received and     
              generated messages.
        \item For AISL: count the amount of \alert{transitions} between states that are followed.
    \end{itemize}
\end{frame}

\begin{frame}{Case Study: Autobahn WebSocket Server}
    \begin{itemize}
        \item \emph{WebSocket} protocol (RFC 6455)
        \begin{itemize}
            \item Bidirectional communication layer between browser and web server.
            \item Starts with a HTTP-like \emph{handshake}, then both sides can send message 
                  \emph{frames}.
            \item One message may be divided over several frames.
        \end{itemize}
        \bigskip
        \item Test subject: an \emph{echo server} that repeats what is said to it.
        \item Implementation: Autobahn WebSocket framework.
        \item Test duration: 500 steps.
    \end{itemize}
\end{frame}

\begin{frame}{WebSocket Results}
    \begin{itemize}
        \item Problem encountered: WebSocket is an \emph{asynchronous protocol}.
        \begin{itemize}
            \item Client and server may send a message at the same time.
            \item How to determine which came first?
        \end{itemize}
        \item The whole specification was covered, with the exception of:
        \begin{itemize}
            \item Protocol features the server never chose to utilise.
            \item A field only present with chance $1 \over 127$, due to the `dumb' random 
                  testing strategy:
        \end{itemize}
    \end{itemize}
    \centerline{\includegraphics[scale=0.45]{websocket-payload}}
\end{frame}

\begin{frame}{Case Study: Courier IMAP Server}
    \begin{itemize}
        \item \emph{Internet Message Access Protocol}, version 4 (RFC 3501)
        \begin{itemize}
            \item Allows e-mail clients to access remote mailboxes.
            \item State machine involves authentication, mailbox selection and message manipulation 
                  phases.
        \end{itemize}
        \bigskip
        \item Test subject: a prepared inbox filled with 50 messages; hard-coded password.
        \item Implementation: Courier IMAP Server.
        \item Test duration: 500 steps.
    \end{itemize}
\end{frame}

\begin{frame}{IMAP State Machine}
	\centerline{\includegraphics[scale=0.4]{imap-diagram}}
\end{frame}

\begin{frame}{IMAP Results}
    \begin{itemize}
        \item Possible bug in Courier found when attempting to delete the main inbox.
        \item Parts of the FETCH command message were never covered:
        \begin{itemize}
            \item After quickly being tried once, it was only repeated one second time.
            \item Caused by the greedy random traversal strategy and the many alternative paths.
        \end{itemize}
        \item The STORE command was never send:
        \begin{itemize}
            \item State from which STORE could be executed was reached only once.
            \item Could be prevented by ``looking ahead'' beyond neighbour states.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Conclusion}
%1
    \begin{itemize}
        \item Protocol specification languages can be a useful tool to aid implementers.
        \item Currently available languages are not suitable for conformance test derivation.
        \item The APSL language offers a framework that attempts to solve this problem.
        \item Case studies show the current implementation still has considerable limitations.
        \bigskip
        \item However, it is shown that this method of test derivation can be practical.
    \end{itemize}
\end{frame}

\end{document}
