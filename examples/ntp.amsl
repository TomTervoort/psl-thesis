# Network Time Protocol version 4. Based on RFC 5905.

message module ntp

# Signed integer types.

type Int64 is Integer(min=-2^63, max = 2^63-1)
default Int64 as BigEndian(signed=true, length=64)

type Int32 is Integer(min=-2^31, max = 2^31-1)
default Int32 as BigEndian(signed=true, length=32)

type Int16 is Integer(min=-2^15, max = 2^15-1)
default Int16 as BigEndian(signed=true, length=16)


# Time formats.

record Date with
    era      is Int32
    offset   is Int32
    fraction is Int64
end

record Timestamp with
    seconds  is Int32
    fraction is Int32
end

record ShortTime with
    seconds  is Int16
    fraction is Int16
end
