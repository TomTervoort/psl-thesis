message module websocket

#type UriPath is Text(pattern=/(\/([a-zA-Z0-9\-\._~]*|%[0-9a-fA-F]{2})*)+/)
type UriPath is Text(pattern=/\/[a-zA-Z0-9\-\._~\/]*/)

codec Bit is BoolBits(truth_string=b'1')
codec BE  is BigEndian(signed=false)

default Text as FixedCountText(encoding='utf-8')

message ClientOpenHandshake with
    _method  is Text(value='GET ')
    resource is UriPath(exclude_pattern=/ /) as TerminatedText(encoding='utf-8', terminator=' ')
    protocol is Text(value='HTTP/1.1')       as TerminatedText(encoding='utf-8', terminator="\r\n")

#    host         is HttpHeader(key=/host/, val=/[a-zA-Z0-9]([a-zA-Z0-9\.\-]{0,254}[a-zA-Z0-9])?*(:[0-9]{1,5})?/)
    host         is HttpHeader(key=/host/, val=/[a-zA-Z0-9\.]+/)
    upgrade      is HttpHeader(key=/upgrade/, val=/websocket/)
    connection   is HttpHeader(key=/connection/, val=/upgrade/)
    key          is HttpHeader(key=/sec-websocket-key/, val=/[A-Za-z0-9\+\/]{22}==/)
    version      is HttpHeader(key=/sec-websocket-version/, val=/13/)

    _end         is Text(value="\r\n")
end

message ServerOpenHandshake with
    status_line is Text(pattern=/HTTP\/1\.1 101 [ -~]*/, exclude_pattern=/\r\n/) as TerminatedText(encoding='ascii', terminator="\r\n")
    headers is Text(exclude_pattern=/\r\n\r\n/) as TerminatedText(encoding='ascii', terminator="\r\n\r\n")

#    upgrade     is HttpHeader(key=/upgrade/, val=/websocket/)
#    connection  is HttpHeader(key=/connection/, val=/upgrade/)
#    accept      is HttpHeader(key=/sec-websocket-accept/, val=/[A-Za-z0-9\+\/]{27}=/)
#
#    _end        is Text(value="\r\n")
end

record HttpHeader(key, val) with
    name  is Text(pattern=key, exclude_pattern=/:/)    as TerminatedText(encoding='ascii', terminator=':')
    value is Text(pattern=val, exclude_pattern=/\r\n/) as TerminatedText(encoding='ascii', terminator="\r\n")
end

record Frame(first, final, must_mask) with
    fin           is Bool(value=final) as Bit
    _reserved     is Binary(value=b'000')
    opcode        is Optional(subject=Opcode, is_empty=!first)
                        as OptionalCodec(subject_codec=BE(length=4), null_string=b'0000')
    mask is Bool(value=true if must_mask else null) as Bit
    payload_len   is Integer(min=0, max=127) as BE(length=7)
    payload_len16 is Optional(is_empty=payload_len != 126, subject=Integer(min=0, max=2^16-1))
                        as OptionalCodec(subject_codec=BE(length=16))
    payload_len64 is Optional(is_empty=payload_len != 127, subject=Integer(min=0, max=2^16-1))
                         as OptionalCodec(subject_codec=BE(length=64))
    mask_key      is Optional(is_empty=!mask, subject=Binary(length=32))
                         as OptionalCodec(subject_codec=FixedLengthBinary)
    payload_data  is Payload(tag=opcode if opcode != null else bin_frame, 
                             length=8 * (payload_len   if payload_len <= 125 else 
                                         payload_len16 if payload_len == 126 else
                                         payload_len64))
end

message MessageFrame with
    frame is Frame(first=true, final=true, must_mask = false)
end

message MessageStartFrame with
    frame is Frame(first=true, final=false, must_mask = false)
end

message ContinuationFrame with
    frame is Frame(first=false, final=false, must_mask = false)
end

message MessageEndFrame with
    frame is Frame(first=false, final=true, must_mask = false)
end

message MaskedMessageFrame with
    frame is Frame(first=true, final=true, must_mask = true)
end

message MaskedMessageStartFrame with
    frame is Frame(first=true, final=false, must_mask = true)
end

message MaskedContinuationFrame with
    frame is Frame(first=false, final=false, must_mask = true)
end

message MaskedMessageEndFrame with
    frame is Frame(first=false, final=true, must_mask = true)
end

enum Opcode of Integer with
    #text_frame as 1  #b'0001'
    bin_frame  as 2  #b'0010'
    con_close  as 8  #b'1000'
    ping       as 9  #b'1001'
    pong       as 10 #b'1010'
end


union Payload(length) tagged Opcode of
    #text_frame tags Text(utf8_length=length)
    bin_frame  tags Binary(length=length)
    con_close  tags Optional(subject=CloseReason(length=length), is_empty=length == 0) as OptionalCodec(subject_codec=RecordCodec)
    ping       tags Binary(length=length)
    pong       tags Binary(length=length)
end

record CloseReason(length) with
    status_code is Integer(min=1000,max=4999) as BE(length=16)
    reason      is Text(count=(length - 16) // 8, charset='ascii')
end