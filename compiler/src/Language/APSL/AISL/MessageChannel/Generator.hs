{-# LANGUAGE DeriveDataTypeable #-}

-- | A message generator based on AMSL message specifications.
--   It detects message labels by parsing incoming bytes, and transmits randomly generated messages that match the 
--   description.
module Language.APSL.AISL.MessageChannel.Generator (
    Timeout,
    IOWithTrace,
    generatorChannel,
    generatorChannelWithTrace
) where

import Prelude hiding (getContents)

import Language.APSL.Base
import Language.APSL.AMSL.Encoder
import Language.APSL.AMSL.Decoder
import Language.APSL.AMSL.Generator
import Language.APSL.AMSL.Generator.Coverage
import Language.APSL.AMSL.Spec.Module hiding (Module, messages)
import Language.APSL.AISL.MessageChannel
import Language.APSL.AISL.Module

import Network.Socket hiding (sendAll, send)
import Network.Socket.ByteString.Lazy (sendAll, getContents)
import Test.QuickCheck.Gen
import Data.Binary.Get
import Data.Binary.Bits.Get
import Data.Binary.Put
import Data.Binary.Bits.Put
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Writer

import Data.Typeable
import Control.Exception
import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Concurrent.STM.TChan
import Control.Applicative
import           Data.Map (Map)
import qualified Data.Map as M
import           Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B

import Text.PrettyPrint
import Language.APSL.AMSL.PrettyPrint
import Data.Hex


-- | Receive timeout in milliseconds.
type Timeout = Int

-- | The generator-based channel.
generatorChannel :: Module -> GenParams -> Socket -> Timeout -> IO (MessageChannel IO)
generatorChannel mod gp sock timeout = do 
    chan <- generatorChannelWithTrace mod gp sock timeout
    return $ MessageChannel {
        send = dropTrace . send chan,
        receive = dropTrace $ receive chan
    }
 where
    dropTrace = liftM fst . runWriterT

-- | Writer transformer that outputs a trace of message instances that have generated and received during operation.
type IOWithTrace = WriterT [MessageInstance] IO

data DecodeError = DecodeError String 
 deriving (Show, Typeable)

instance Exception DecodeError

-- | A generator channel that also maintains a trace of the generated messages.
generatorChannelWithTrace :: Module -> GenParams -> Socket -> Timeout -> IO (MessageChannel IOWithTrace)
generatorChannelWithTrace mod gp sock timeout = do
    inbox <- newTQueueIO
    inp <- getContents sock
    forkIO $ receiveLoop inbox $ B.toChunks inp

    return $ MessageChannel {
        send = \label -> do
            lift $ putStrLn $ "Sent " ++ label
            let Just (msg, env) = M.lookup label msgs
            inst@(MessageInstance tn r x) <- lift $ generate $ genMessage gp env msg
            -- lift $ putStrLn $ render $ prettyPrintMessage inst
            tell [inst]
            let bytes = runPut $ runBitPut $ encodeRecord arbitraryGenerator env tn r x
            -- lift $ putStrLn $ show $ hex bytes
            lift $ sendAll sock bytes,

        receive = do
            result <- lift $ race (threadDelay $ timeout * 1000) (atomically $ peekTQueue inbox)
            case result of 
                Left ()   -> do 
                    -- lift $ putStrLn "Message timeout"
                    return Nothing
                Right (Left (label, inst)) -> do
                    lift $ putStrLn $ "Received " ++ label
                    tell [inst]
                    lift $ atomically $ readTQueue inbox
                    return $ Just label
                Right (Right err) -> lift $ throwIO err
    } 
 where
    msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- messages mod]

    getMsg (l, (Message tn r, env)) = runBitGet (decodeRecord env tn r) >>= \x -> return (l, MessageInstance tn r x)

    receiveLoop inbox bs = receiver inbox bs $ runGetIncremental $ foldr1 (<|>) $ reverse $ map getMsg $ M.assocs msgs
    receiver inbox bs (Done b' _ x) = atomically (writeTQueue inbox $ Left x) >> receiveLoop inbox (b':bs)
    -- TODO: handle invalid incoming messages in a way other than crashing.
    receiver inbox _ (Fail _ _ err)  = atomically (writeTQueue inbox $ Right $ DecodeError err)
    receiver inbox [] _ = return ()
    receiver inbox (b:bs) (Partial f) = receiver inbox bs $ f $ Just b
    
    