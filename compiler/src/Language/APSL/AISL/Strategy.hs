{-# LANGUAGE RecordWildCards #-}
-- | Representing strategies for exploring an LTS by sending messages.
module Language.APSL.AISL.Strategy where

import Language.APSL.AISL.LTS
import Language.APSL.AISL.LTS.Traversal
import Language.APSL.AISL.MessageChannel

import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M


import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.State

import Debug.Trace


-- | The state of the exploration process. Based on this a decision needs to be made.
data ExploreState s = ExploreState {
    -- | The complete LTS of the tested system.
    lts :: LTS,

    -- | The possible states the system may be in.
    potentialStates :: Set StateID,

    -- | The latest received message; or Nothing in case there is none.
    receivedMessage :: Maybe MessageLabel,

    -- | Additional user-defined state. It is maintained after a reset and may be updated at each decision point.
    userState :: s
}

-- | How to act at a 'decision point' in the testing process. r is a type representing final test results.
data Decision r
    = SendMessage MessageLabel -- ^ Send a particular message.
    | AwaitMessage             -- ^ Wait until a new message comes in, or a timeout occurrs.
    | Finish r                 -- ^ Finished testing.

-- | Helper that provides the states that could potentially (not certainly when currently in more than one state) 
--   be reached by sending a particular message.
sendOptions :: ExploreState s -> Set (MessageLabel, StateID)
sendOptions ExploreState{..} = S.fromList $ concat $ map neighbours $ S.toList potentialStates
 where
    neighbours state = case M.lookup state $ transitions lts of
        Nothing -> []
        Just ts -> [(msg, next) | (act, nexts) <- M.assocs ts, 
                                  msg <- receiveFromAction (canReceive ts) act,
                                  next <- nexts]

    receiveFromAction expected act = case act of
        Receive msg       -> [msg]
        ReceiveUnexpected -> [msg | msg <- S.elems $ possibleMessages lts, not $ msg `S.member` expected]
        Send _            -> []
        Internal          -> []

    canReceive ts = S.fromList [msg | Receive msg <- M.keys ts]

receiveOptions :: ExploreState s -> Set (MessageLabel, StateID)
receiveOptions ExploreState{..} = S.fromList $ concat $ map neighbours $ S.toList potentialStates
 where
    neighbours state = case M.lookup state $ transitions lts of
        Nothing -> []
        Just ts -> [(msg, next) | (Send msg, nexts) <- M.assocs ts, next <- nexts]


-- | A strategy is defined by how to act at a decision point, and how to update the state.
--   The parameter s represents the state, and r the test report when finished.
type Strategy s r = ExploreState s -> (Decision r, s)

-- | Execute the strategy within the Traversal monad, given some message channel.
runStrategy :: Monad m => MessageChannel m -> Strategy s r -> TraversalT s m r
runStrategy chan strat = do
    lts <- getLTS
    receivedMessage <- lift (receive chan)
    maybe (return ()) sent receivedMessage
    potentialStates <- possibleStates
    -- trace (show potentialStates) $ return ()
    userState <- get
    let (dec, s') = strat ExploreState{..}
    put s'
    case dec of
        Finish x        -> return x
        SendMessage msg -> received msg >> lift (send chan msg) >> runStrategy chan strat
        AwaitMessage    -> do 
            runStrategy chan strat
        