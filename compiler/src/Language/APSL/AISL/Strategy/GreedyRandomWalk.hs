{-# LANGUAGE RecordWildCards, PatternGuards #-}

-- | A 'greedy random walk' strategy: it randomly selects a neighbouring state to visit, but always picks a yet 
--   unvisited state over others.
--   The strategy terminates after a predetermined amount of steps.
module Language.APSL.AISL.Strategy.GreedyRandomWalk (greedyRandomWalk, GRWState, initGRWState, GRWEndReason(..)) where

import Language.APSL.AISL.Strategy
import Language.APSL.AISL.LTS

import System.Random
import Control.Arrow (first)
import           Data.Set (Set)
import qualified Data.Set as S


data GRWState g = GRWState {
    gen :: g,
    visited :: Set StateID,
    stepsLeft :: Integer
}

-- TODO: move this (along with termination check) to Strategy.
data GRWEndReason
    = ErrorFound
    | InExitState
    | StepLimitreached
    deriving (Eq, Ord, Show)

-- | Initialize the state, given an RNG, and a step count after which the strategy should terminate.
initGRWState :: RandomGen g => g -> Integer -> GRWState g
initGRWState g count = GRWState {gen = g, visited = S.empty, stepsLeft = count}


-- TODO: handle cases where both receiving and sending is possible (currently sending is always prioritized).
greedyRandomWalk :: RandomGen g => Strategy (GRWState g) GRWEndReason
greedyRandomWalk s@ExploreState{..} 
    | Just reason <- terminateCheck = (Finish reason, userState)
    | otherwise = 
        (dec, userState{gen       = gen', 
                        visited   = visited userState `S.union` potentialStates,
                        stepsLeft = stepsLeft userState - 1})
 where
    (done, notDone) = S.partition ((`S.member` visited userState) . snd) $ sendOptions s
    options 
        | S.null notDone = done
        | otherwise = notDone
    (dec, gen') 
        | S.null options = (AwaitMessage, gen userState)
        | otherwise = first (SendMessage . pickMessage) $ randomR (0, S.size options - 1) $ gen userState
    pickMessage i = fst $ S.toList options !! i  -- S.elemAt i options

    terminateCheck 
        | S.null potentialStates = Just ErrorFound
        | potentialStates == S.singleton Exit = Just InExitState
        | stepsLeft userState <= 0 = Just StepLimitreached
        | otherwise = Nothing
