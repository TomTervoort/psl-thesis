{-# LANGUAGE FunctionalDependencies, FlexibleInstances, RecordWildCards, PatternGuards, GeneralizedNewtypeDeriving #-}

-- | A (transformer and state) monad for examining and traversing an LTS by providing the actions that are observed.
module Language.APSL.AISL.LTS.Traversal (
    Traversal,
    TraversalT,
    Coverage (..),
    traverseLTS,
    traverseLTST,
    traverseLTSTWithCoverage,
    possibleStates,
    getLTS,
    getCoverage,
    hasExited,
    received,
    sent,
    coverAmount,
    printCoverage
) where

import Language.APSL.AISL.LTS

import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M

import Data.Functor.Identity
import Data.Monoid
import Data.Ratio

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State (StateT)
import Control.Monad.State
import Control.Monad.IO.Class

-- | State monad that represents a traversal along an LTS.
type Traversal s = TraversalT s Identity

-- | Traversal monad tranformer.
newtype TraversalT s m a = Traversal { unTraversal :: StateT (LTSPos, s) m a }
                            deriving (Functor, Applicative, Monad)

-- | Total transition coverage.
newtype Coverage = Coverage { unCoverage :: Set Transition }

-- The keys within the map are the currently possible states; they are each associated with the transitions that have 
-- been covered to get there. An empty map indicates an unexpected action was observed, meaning the modelled system is
-- in an invalid state according to the LTS.
data LTSPos = LTSPos LTS (Map StateID Coverage)

-- instance Monad m => Monad (TraversalT s m) where
--     return x = Traversal $ return x
--     Traversal m >>= f = Traversal $ m >>= unTraversal . f
    
instance Monad m => MonadState s (TraversalT s m) where
    state f = Traversal $ state $ \(pos, s) -> let (x, s') = f s in (x, (pos, s'))

instance MonadTrans (TraversalT s) where
    lift = Traversal . lift

instance MonadIO m => MonadIO (TraversalT s m) where
    liftIO = lift . liftIO

instance Monoid Coverage where
    mempty = Coverage S.empty
    mappend (Coverage a) (Coverage b) = Coverage $ S.union a b

coverTransition :: Transition -> Coverage -> Coverage
coverTransition t (Coverage s) = Coverage $ S.insert t s

intersectCoverage :: Coverage -> Coverage -> Coverage
intersectCoverage (Coverage a) (Coverage b) = Coverage $ S.intersection a b


-- | Apply the traversal to an LTS and an initial state, which may be updated by the process.
traverseLTS :: LTS -> Traversal s a -> s -> (a, s)
traverseLTS lts t s = runIdentity $ traverseLTST lts t s

-- | Transformer version of traverseLTS.
traverseLTST :: Monad m => LTS -> TraversalT s m a -> s -> m (a, s)
traverseLTST lts@LTS{..} t s = do 
    let startPos = LTSPos lts (M.singleton initState mempty)
    (x, (_, s')) <- runStateT (unTraversal $ handleInternalActions >> t) (startPos, s)
    return (x, s')

-- | Traversal function that includes the eventual coverage with the output.
traverseLTSTWithCoverage :: Monad m => LTS -> TraversalT s m a -> s -> m (a, s, Coverage)
traverseLTSTWithCoverage lts t s = liftM (\((r, t), s) -> (r, s, t)) $ traverseLTST lts t' s
 where 
    t' = do result <- t
            cover <- getCoverage
            return (result, cover)

-- | Provides the set of states the system might currently be in.
--   Also takes into account any internal actions that may or may not have taken place
possibleStates :: Monad m => TraversalT s m (Set StateID)
possibleStates = Traversal $ liftM (\(LTSPos _ states, _) -> M.keysSet states) get

-- | View the entire LTS that is being traversed.
getLTS :: Monad m => TraversalT s m LTS
getLTS = Traversal $ liftM (\(LTSPos lts _,_) -> lts) get

-- | Get the total coverage. In case of nondeterminism only the transitions of which it is certain that they have been
--   touched will be included.
getCoverage :: Monad m => TraversalT s m Coverage
getCoverage = Traversal $ liftM (\(LTSPos _ states, _) -> combined $ M.elems states) get
 where
    combined [] = mempty
    combined xs = foldr1 intersectCoverage xs

-- | Whether currently in the exit state, or Nothing if that is pLTSPosossible but not certain.
hasExited :: Monad m => TraversalT s m (Maybe Bool)
hasExited = liftM check possibleStates
 where
    check states 
        | states == S.singleton Exit = Just True
        | Exit `S.member` states     = Nothing
        | otherwise                  = Just False

-- | Indicate that a certain message has been received from the system, causing the graph to be traversed accordingly.
received :: Monad m => MessageLabel -> TraversalT s m ()
received msg = stateUpdate received' >> handleInternalActions
 where
    received' ts = case M.lookup (Receive msg) ts of
        Just newStates -> (Receive msg, newStates)
        Nothing        -> (ReceiveUnexpected, M.findWithDefault [] ReceiveUnexpected ts)

-- | Indicate that a certain message has just been sent to the system, causing the graph to be traversed accordingly.
sent :: Monad m => MessageLabel -> TraversalT s m ()
sent msg = stateUpdate (\ts -> (Send msg, M.findWithDefault [] (Send msg) ts)) >> handleInternalActions

stateUpdate :: Monad m => (Transitions -> (Action, [StateID])) -> TraversalT s m ()
stateUpdate f = do
    tss <- liftM transitions getLTS
    let fromState s = let (act, nexts) = f $ M.findWithDefault M.empty s tss in [(s, act, next) | next <- nexts]
    Traversal $ modify $ \(LTSPos lts states, s) ->
        let newStates = foldr (combineSteps states) M.empty $ concatMap fromState $ M.keys states
         in (LTSPos lts newStates, s)
 where
    
    combineSteps old t@(from, act, to) new = 
        let newCoverage = coverTransition t $ M.findWithDefault mempty from old
         in case M.lookup to new of
            Nothing -> M.insert to newCoverage new
            Just c  -> M.insert to (intersectCoverage newCoverage c) new

-- Extend the set of possible states based on internal actions that may or may not have taken place.
handleInternalActions :: Monad m => TraversalT s m ()
handleInternalActions = do
    (LTSPos _ old, _) <- Traversal get
    stateUpdate $ \ts -> (Internal, M.findWithDefault [] Internal ts)
    new <-
        Traversal $ state $ \(LTSPos lts updated, s) -> 
            let new = M.unionWith intersectCoverage old updated in (new, (LTSPos lts new, s))

    -- Iterate updater until the option set no longer expands.
    when (M.keys old /= M.keys new) handleInternalActions

-- | Compute the fraction of an LTS that has been covered.
coverAmount :: LTS -> Coverage -> Rational
coverAmount lts (Coverage cov) = toRational $ S.size cov % S.size (allTransitions lts)

printCoverage :: LTS -> Coverage -> String
printCoverage lts c@(Coverage c') = 
    (showPercentage $ coverAmount lts c) ++ "\n" ++ concatMap p (S.elems $ allTransitions lts)
 where
    showPercentage x = concat [show $ floor $ x * 100, ".", show $ floor (x * 10000) `mod` 100, "%"]
    p trans = (if trans `S.member` c' then "Y " else "N ") ++ show trans ++ "\n"
