module Language.APSL.AISL.MessageChannel where

import Language.APSL.AISL.LTS

-- | A channel through which messages, identified by labels, can be sent and received. It operates within some monad m.
data MessageChannel m = MessageChannel {
	-- | Send a message.
	send :: MessageLabel -> m (),

	-- | Wait to receive one; may fail in case an implementation-dependent timeout occurs.
	receive :: m (Maybe MessageLabel)
}