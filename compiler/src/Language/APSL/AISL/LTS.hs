{-# LANGUAGE RecordWildCards #-}

-- | Representation of a labelled transition system derived from an AISL specification, of which the set of observable 
--   actions consists of 'receive' and 'send' operations for messages with a textual identifier.
module Language.APSL.AISL.LTS where

import           Data.Map (Map)
import qualified Data.Map as M

import           Data.Set (Set)
import qualified Data.Set as S


-- | Identifier of a particular type of message.
type MessageLabel = String

-- | A state can either have a string label, be an intermediate output state, or be an unnamed 'exit' state that is 
--   implicit in the specification.
data StateID = Normal String | Intermediate Integer | Exit
 deriving (Eq, Ord, Show)

-- | Actions are either internal (the 'anytime' construct in AISL), observable send/receive operations, or the 
--   reception of a message for which no other action is defined.
data Action = Send MessageLabel | Receive MessageLabel | Internal | ReceiveUnexpected
    deriving (Eq, Ord, Show)

-- | Outgoing transitions from one state to others, depending on the action that will occur.
--   Nondeterminism is modelled by one action mapping to multiple states.
type Transitions = Map Action [StateID]

-- | A system consists of an initial state and a map of transitions. 
data LTS = LTS {
    initState   :: StateID,
    transitions :: Map StateID Transitions
} deriving (Show)

-- | A single LTS transition is identified by an (action, from, to)-triple.
type Transition = (StateID, Action, StateID)

-- | Set of all possible transitions defined in the LTS.
allTransitions :: LTS -> Set Transition
allTransitions LTS{..} = S.fromList [(from, act, to) | (from, ts) <- M.assocs transitions, 
                                                       (act, tos) <- M.assocs ts,
                                                       to <- tos]

-- | All the message labels that are used in some action within the LTS.
possibleMessages :: LTS -> Set MessageLabel
possibleMessages lts = S.fromList [msg | (_, act, _) <- S.elems $ allTransitions lts, msg <- getMsg act]
 where 
    getMsg (Send msg)    = [msg]
    getMsg (Receive msg) = [msg]
    getMsg _             = []
