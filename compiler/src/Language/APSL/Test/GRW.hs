{-# LANGUAGE PatternGuards #-}
-- | Apply the GreedyRandomWalk strategy, with the standard generator, to a specification and a TCP server implementing
--   the protocol.
module Language.APSL.Test.GRW where

import Language.APSL.AMSL.Generator
import Language.APSL.AMSL.Generator.Coverage (moduleCoverageBase, moduleCoverageAdd, printModuleCoverage)
import Language.APSL.AISL.LTS.Traversal
import Language.APSL.AISL.Module
import Language.APSL.AISL.Compiler (compile)
import Language.APSL.AISL.Strategy
import Language.APSL.AISL.Strategy.GreedyRandomWalk
import Language.APSL.AISL.MessageChannel.Generator

import System.Environment
import Network.Socket
import Network.BSD
import System.Random
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Writer

receiveTimeout :: Timeout
receiveTimeout = 50

strategyStepCount :: Integer
strategyStepCount = 500

testSpec :: Socket -> FilePath -> FilePath -> String -> IO ()
testSpec sock base path actor = do
    rng <- newStdGen
    compileResult <- compile base path

    let m | Left err  <- compileResult = error $ show err
          | Right mod <- compileResult = mod
    let Just lts = lookup actor $ actors m

    chan <- generatorChannelWithTrace m defaultParams sock receiveTimeout
    let trav = runStrategy chan greedyRandomWalk
    ((result, _, ltsCov), msgTrace) <- runWriterT $ 
        traverseLTSTWithCoverage lts trav $ initGRWState rng strategyStepCount

    let msgCov = foldr moduleCoverageAdd (moduleCoverageBase $ map fst $ messages m) msgTrace

    putStrLn $ "Test finished: " ++ show result
    when (result /= ErrorFound) $ do
        putStrLn $ "Message coverage: " ++ printModuleCoverage msgCov
        putStrLn $ "State transition coverage: " ++ printCoverage lts ltsCov
 where
    showPercentage :: Rational -> String
    showPercentage x = concat [show $ floor $ x * 100, ".", show $ floor (x * 10000) `mod` 100, "%"]

testServer :: (HostName, PortNumber) -> FilePath -> FilePath -> String -> IO ()
testServer (host, port) base path actor = do
    sock <- socket AF_INET Stream defaultProtocol
    hostEntry <- getHostByName host
    connect sock $ SockAddrInet port $ hostAddress hostEntry
    testSpec sock base path actor
    close sock

main :: IO ()
main = do
    [host, port, specBase, specPath, actor] <- getArgs
    testServer (host, fromInteger $ read port) specBase specPath actor


websocketTest = testServer ("localhost", 9000) "../test-cases" "websocket.aisl" "WebsocketServer"

imapTest = testServer ("localhost", 143) "../test-cases" "imap.aisl" "IMAPServer"
