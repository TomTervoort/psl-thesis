{-# LANGUAGE GADTs #-}
module Language.APSL.Test.DecodeAndPrint where

import Test.QuickCheck.Gen

import Language.APSL.AMSL.Spec.Module
import Language.APSL.AMSL.Encoder
import Language.APSL.AMSL.Decoder
import Language.APSL.AMSL.PrettyPrint
import Language.APSL.AMSL.Compiler
import Language.APSL.AMSL.Spec.Types
import Language.APSL.AMSL.Generator
import Language.APSL.AMSL.Generator.Regex

import Data.Binary.Get
import Data.Binary.Bits.Get
import Data.Binary.Put
import Data.Binary.Bits.Put

import Text.PrettyPrint
import           Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B
import Control.Monad
import Debug.Trace

gen = arbitraryGenerator

-- Note: this is not a general test, since redundant information may be lost when decoding with codecs such as
-- LengthPrefixInteger or TerminatedUnionList.
decodeEncodeTest :: Module -> Message -> ByteString -> Bool
decodeEncodeTest m (Message tn r) inp = encoded == inp
 where 
    decoded = runGet (runBitGet $ decodeRecord (moduleEnv m) tn r) inp
    encoded = runPut (runBitPut $ encodeRecord gen (moduleEnv m) tn r decoded)

decodeAndPrint :: Module -> Message -> ByteString -> String
decodeAndPrint m (Message tn r) inp = render $ prettyPrint (UserType $ Record tn [] r) decoded
 where decoded = runGet (runBitGet $ decodeRecord (moduleEnv m) tn r) inp

decodeAndPrintTest :: IO ()
decodeAndPrintTest = do
    Right spec <- compile "../examples" "test.amsl"
    inp <- B.readFile "../examples/test_input.bin"
    let msg = head $ messages spec
    unless (decodeEncodeTest spec msg inp) $ fail "decodeEncodeTest failed"
    putStrLn $ decodeAndPrint spec msg inp

generateAndPrintTest :: String -> IO ByteString
generateAndPrintTest file = do
    compiled <- compile "../examples" file
    case compiled of
        Left err -> error $ show err
        Right spec -> do
            let msg = head $ messages spec
            MessageInstance tn r x <- generate $ genMessage defaultParams{size = Just 20, intBounds = Just (-100, 100)} (moduleEnv spec) msg
            putStrLn $ render $ prettyPrint (UserType $ Record tn [] r) x
            return $ runPut $ runBitPut $ encodeRecord gen (moduleEnv spec) tn r x

