{-# LANGUAGE RecordWildCards, TupleSections #-}
module Language.APSL.Test.InteractiveTraversal where

import Language.APSL.Base
import Language.APSL.AMSL.Spec.Module (messageName)
import Language.APSL.AISL.Module
import Language.APSL.AISL.Compiler
import Language.APSL.AISL.LTS
import Language.APSL.AISL.LTS.Traversal
import Language.APSL.AISL.Strategy
import Language.APSL.AISL.Strategy.GreedyRandomWalk
import Language.APSL.AISL.MessageChannel

import Control.Monad
import Control.Monad.Trans.Class
import System.Random
import           Data.Set (Set)
import qualified Data.Set as S

interactive :: (TraversalT () IO (), ())
interactive = (run, ())
 where
    run = do
        states <- possibleStates
        lift $ putStr "Possible states: " >> print states
        inp <- lift $ putStr ">> " >> getLine
        case inp of
            'S':' ':msg -> sent msg
            'R':' ':msg -> received msg
            _ -> lift $ putStrLn "Please input S or R, followed by a space and message label."
        run

fakeChannel :: MessageChannel IO
fakeChannel = MessageChannel {
    send     = \msg -> putStrLn $ "Sent " ++ msg,
    receive  = putStr "Receiving: " >> liftM emptyIsNothing getLine
} where
    emptyIsNothing ""  = Nothing
    emptyIsNothing str = Just str

strategy :: Strategy s r -> TraversalT s IO r
strategy = runStrategy fakeChannel

grwTest :: RandomGen g => g -> (TraversalT (GRWState g) IO (), GRWState g)
grwTest gen = (void $ strategy greedyRandomWalk, initGRWState gen 100)

compileAndTraverse :: FilePath -> FilePath -> (TraversalT s IO r, s) -> IO r
compileAndTraverse dir path (trav, state) = do
    compiled <- compile dir path
    case compiled of
        Left err  -> fail $ "Compile error: " ++ show err
        Right mod@Module{..} -> do
            putStr "Module: "   >> print moduleName 
            putStr "Messages: " >> print (map (unTypeName . messageName . fst) messages)
            putStr "Actors: "   >> print (map fst actors)
            
            actor <- putStr "Pick an actor: " >> getLine
            case lookup actor actors of
                Nothing  -> fail "Invalid actor."
                Just lts -> liftM fst $ traverseLTST lts trav state

testWebSocket = newStdGen >>= compileAndTraverse "../examples" "websocket.aisl" . grwTest

