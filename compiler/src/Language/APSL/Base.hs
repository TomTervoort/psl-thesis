{-# LANGUAGE GeneralizedNewtypeDeriving, RankNTypes, EmptyDataDecls, PatternGuards #-}
module Language.APSL.Base where

import Data.Word
import Data.Char
import Data.String

import           Data.Map (Map)
import qualified Data.Map as M
import Data.Array (inRange)

newtype TypeName = TypeName {unTypeName :: String}    deriving (Eq, Ord, IsString)
type CodecName = TypeName
newtype FieldName = FieldName {unFieldName :: String} deriving (Eq, Ord, IsString)
newtype ParamName = ParamName {unParamName :: String} deriving (Eq, Ord, IsString)

instance Show TypeName where show = unTypeName
instance Show FieldName where show = unFieldName
instance Show ParamName where show = unParamName

-- | A type with no values.
data Void

data ExtData = TODO

type Charset = [(Char, Char)]

-- | Mapping of character set names to (inclusive) code point ranges. Note that a character set is not the same as an 
--   encoding, but that certain encodings may only encode members of a certain set.
charsets :: Map String Charset
charsets = 
    M.fromList [
        ("unicode", [(minBound, maxBound)]),
        ("ascii", [(chr 0, chr 127)]),
        ("printable_ascii", [(chr 20, chr 126)])
    ]

inCharset :: String -> Char -> Bool
inCharset set c | Just ranges <- M.lookup set charsets = any (flip inRange c) ranges
                | otherwise = error $ "Unknown charset: " ++ set
