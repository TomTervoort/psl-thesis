{-# LANGUAGE RecordWildCards, GADTs, OverloadedStrings, PatternGuards #-}
-- | Tool for measuring how much of a message specification is covered by actual messages.
--   With the current measure, the branches of unions, values of enums and the presense of Optional fields are 
--   measured.
module Language.APSL.AMSL.Generator.Coverage (
    Coverage (..),
    coverAmount,
    messageCoverage,
    combineCoverage,
    printCoverage,
    ModuleCoverage (..),
    moduleCoverageBase,
    moduleCoverageAdd,
    moduleCoverAmount,
    printModuleCoverage
) where

import Language.APSL.Base
import Language.APSL.AMSL.Spec.Expression
import Language.APSL.AMSL.Spec.Types
import Language.APSL.AMSL.Spec.TypeCodec
import Language.APSL.AMSL.Spec.Module

import Text.PrettyPrint

import Control.Arrow (first)
import Data.Maybe
import Data.Ratio
import           Data.Map (Map)
import qualified Data.Map as M

-- | Represents a message specification as a tree, with leafs that can be marked as being covered.
--   Both sum and product types are represented by branches, but in the latter case a single message can only cover one
--   direction.
data Coverage
    = UncoveredLeaf
    | CoveredLeaf
    | Node Coverage Coverage
    | Label TypeName Coverage
    deriving (Show)

-- | The fraction of leaves being covered.
coverAmount :: Coverage -> Rational
coverAmount UncoveredLeaf = 0
coverAmount CoveredLeaf  = 1
coverAmount (Label _ c) = coverAmount c
coverAmount (Node a b)   = (numerator ca + numerator cb) % (denominator ca + denominator cb)
 where (ca, cb) = (coverAmount a, coverAmount b)

printCoverage :: Coverage -> String
printCoverage c = showPercentage (coverAmount c) ++ "\n" ++ render (pp c)
 where
    pp UncoveredLeaf           = text "N"
    pp CoveredLeaf             = text "Y"
    pp (Label (TypeName l) c)  = text l $+$ pp c
    pp (Node l r)              = text "-" $+$ (text "." $$ nest 2 (pp l)) $+$ (text "." $$ nest 2 (pp r))

-- | Determines how much a specification of a single message covers.
messageCoverage :: MessageInstance -> Coverage
messageCoverage (MessageInstance tn r x) = typeCoverage (UserType $ Record tn [] r, Arguments []) $ Just x

typeCoverage :: (Type a, Arguments) -> Maybe a -> Coverage
typeCoverage (ty, args) mx = case ty of
    BasicType Optional                           -> optionalCoverage mx
    UserType  (Record tn _ r)                    -> Label tn $ recordCoverage r mx
    UserType  (Union tn _ _ u)                   -> Label tn $ unionCoverage u mx
    UserType  (Enumeration tn tt (EnumValues e)) -> Label tn $ enumCoverage tt e mx
    UserType  (TypeAlias _ ty' _)                -> typeCoverage (ty', args) mx
    _ | Just _ <- mx                             -> CoveredLeaf
      | otherwise                                -> UncoveredLeaf
    
 where
    tn = typeName ty

    recordCoverage :: Record a -> Maybe a -> Coverage
    recordCoverage Empty Nothing   = UncoveredLeaf
    recordCoverage Empty (Just ()) = CoveredLeaf
    recordCoverage (Field _ FieldTypeCodec{..} r) mxs = 
        Node (typeCoverage (fieldType, fieldTypeArgs) $ fmap fst mxs) (recordCoverage r $ fmap snd mxs)

    unionCoverage :: Union t a -> Maybe a -> Coverage
    unionCoverage (Option _ _ FieldTypeCodec{..} u) mx = 
        let (xl, xr) = case mx of
                Nothing        -> (Nothing, Nothing)
                Just (Left x)  -> (Just x, Nothing)
                Just (Right x) -> (Nothing, Just x)
            cl = typeCoverage (fieldType, fieldTypeArgs) xl
         in 
            case u of
                None -> cl
                _    -> Node cl $ unionCoverage u xr

    enumCoverage :: TagType a -> [(FieldName, a)] -> Maybe a -> Coverage
    enumCoverage tt ((_, y):ys) mx =
        let leaf | Just x <- mx, compareTags tt x y = CoveredLeaf
                 | otherwise                        = UncoveredLeaf
         in if null ys then leaf else Node leaf $ enumCoverage tt ys mx

    optionalCoverage :: Maybe (ContainerValue Maybe) -> Coverage
    optionalCoverage mx | Just (TypeExpArg sty stargs) <- lookup "subject" $ unArguments args =
        let (cl, xr) = case mx of
                Nothing                                  -> (UncoveredLeaf, Nothing)
                Just (ContainerValue _ Nothing)          -> (CoveredLeaf, Nothing)
                Just (ContainerValue sty' (Just x)) 
                    | Just SameType <- sameType sty sty' -> (UncoveredLeaf, Just x)
         in Node cl $ typeCoverage (sty, stargs) xr


-- | Join the coverage of multiple instances of the same message or identically labelled structures.
combineCoverage :: Coverage -> Coverage -> Coverage
combineCoverage = (<++>)
 where
    Label la a      <++> Label lb b    | la == lb = Label la $ a <++> b
    Node ll lr      <++> Node rl rr    = Node (ll <++> rl) (lr <++> rr)
    CoveredLeaf     <++> CoveredLeaf   = CoveredLeaf
    UncoveredLeaf   <++> CoveredLeaf   = CoveredLeaf
    CoveredLeaf     <++> UncoveredLeaf = CoveredLeaf
    UncoveredLeaf   <++> UncoveredLeaf = UncoveredLeaf
    a               <++> b             = error "Incompatible coverage trees."
  

-- | Maintain coverage for the various messages within a module.
data ModuleCoverage = ModuleCoverage {labelCoverage :: Map TypeName Coverage, msgCoverage :: Map TypeName Coverage}

-- Extend coverage of a certain message with the node coverage of other messages, and vice versa.
applyLabelledCoverage :: Coverage -> Map TypeName Coverage -> (Coverage, Map TypeName Coverage)
applyLabelledCoverage (Label l n) m 
    | Just n' <- M.lookup l m = 
        let (new, m') = applyLabelledCoverage (combineCoverage n n') m
         in (Label l new, M.insert l new m')
    | otherwise = 
        let (n', m') = applyLabelledCoverage n m
         in (Label l n', M.insert l n' m')
applyLabelledCoverage (Node l r) m =
    let (l', m1) = applyLabelledCoverage l m
        (r', m2) = applyLabelledCoverage r m1
     in (Node l' r', m2)
applyLabelledCoverage leaf m = (leaf, m)


-- | 0% module coverage that can be extended with moduleCoverageAdd. The argument contains the messages in a (AMSL or 
--   AISL) module.
moduleCoverageBase :: [Message] -> ModuleCoverage
moduleCoverageBase msgs = ModuleCoverage M.empty $ M.fromList [(tn, recordCoverage tn r) | Message tn r <- msgs]
 where recordCoverage tn r = typeCoverage (UserType $ Record tn [] r, Arguments []) Nothing

-- | Add the coverage of a particular message to the total module coverage.
moduleCoverageAdd :: MessageInstance -> ModuleCoverage -> ModuleCoverage
moduleCoverageAdd msg@(MessageInstance tn _ _) (ModuleCoverage lCov base) = 
    let (msgCov, lCov') = applyLabelledCoverage (messageCoverage msg) lCov
     in ModuleCoverage lCov' $ M.insert tn msgCov base

-- | Cover fraction for all messages.
moduleCoverAmount :: ModuleCoverage -> Rational
moduleCoverAmount (ModuleCoverage _ c) = sum $ map ((/ fromIntegral (M.size c)) . coverAmount) $ M.elems c

printModuleCoverage :: ModuleCoverage -> String
printModuleCoverage c@(ModuleCoverage _ c') = showPercentage (moduleCoverAmount c) ++ "\n" ++ concatMap p (M.assocs c')
 where
    p (TypeName tn, cov) = tn ++ ": " ++ printCoverage cov ++ "\n"

showPercentage :: Rational -> String
showPercentage x = concat [show $ floor $ x * 100, ".", show $ floor (x * 10000) `mod` 100, "%"]