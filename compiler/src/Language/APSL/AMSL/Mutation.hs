{-# LANGUAGE RankNTypes, GADTs #-}
module Language.APSL.AMSL.Mutation where 

import Language.APSL.Base
import Language.APSL.AMSL.Spec.Types

type Mutator = forall a. Type a -> a -> a

-- | Applies a mutation to a record field with a particular name.
alterRecordField :: FieldName -> Mutator -> Mutator
alterRecordField name mfield (UserType (Record tn ps r)) x = alter' r x
 where 
 	alter' :: Record a -> a -> a
 	alter' Empty () = ()
 	alter' (Field n ftc r) (x,xs) | name == n = (mfield (fieldType ftc) x, xs)
 								  | otherwise = (x, alter' r xs)

alterRecordField _ _ _ x = x