-- TODO
module Language.APSL.AMSL.Validator where

import Language.APSL.AMSL.Spec.Module
import Language.APSL.AMSL.Spec.Types
import Language.APSL.AMSL.Spec.Expression


validateModule :: Module -> Either ValidationError ()
validateModule _ = Right ()

validateType   :: Type a -> Either ValidationError ()
validateType _ = Right ()

data ValidationError = ValidationError deriving (Show)