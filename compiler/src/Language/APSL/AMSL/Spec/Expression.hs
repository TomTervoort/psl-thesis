{-# LANGUAGE ParallelListComp, RankNTypes, KindSignatures, GADTs #-}

-- | Expressions that can be used within type/codec parameters and a method of computing them.
module Language.APSL.AMSL.Spec.Expression where

import Language.APSL.Base

import Data.BitString.BigEndian (BitString)
import qualified Data.BitString.BigEndian as B

import Control.Monad
import Data.Ord
import Data.List

-- | An argument list that parametrizes a type or codec. To prevent circular imports, it ought to be parametrized by 
--   the /Type/ and /Codec/ types.
newtype Arguments' ty co = Arguments {unArguments :: [(ParamName, Argument' ty co)]}

-- | A single argument. Can be a normal, or a type/codec expression.
data Argument' ty co where
    ExpArg      :: forall a ty co. Expression               -> Argument' ty co
    TypeExpArg  :: forall a ty co. ty a -> Arguments' ty co -> Argument' ty co
    CodecExpArg :: forall a ty co. co   -> Arguments' ty co -> Argument' ty co

-- | The result of computing a plain expression within some environment.
data ExpValue
    = ENull 
    | EBool Bool
    | EInt Integer
    | EBinary BitString
    | EText String
    | ERegex String
    | EList [ExpValue]
    | EEnum TypeName FieldName
    deriving (Eq, Show)

-- | Represents a symbolic expression that yields a value of type 'ExpValue' and may contain variables identified by a 
--   field name. Does not include type or codec expressions.  Note that these expressions are dynamically typed, and 
--   that invalid expressions such as @5 * true@ can be represented, but will result in failure when evaluated.
data Expression 
    = Constant ExpValue
    | Var FieldName
    | Listed [Expression]
    | FieldAccess FieldName FieldName [FieldName]
    -- ^ @a.b.c.d@ as @FieldAccess a b [c,d]@.
    | Operator Expression BinaryOperator Expression
    | Conditional Expression Expression Expression
    -- ^ @y if c else n@ as @Conditional c y n@.
    deriving (Show)

-- | Binary operators that can work on certain types of expressions. 
data BinaryOperator = BAnd | BOr | BXor | BEq | BLess | BConcat | BPlus | BMinus | BMul | BDiv | BMod | BPow
    deriving (Eq, Show)

-- | Environment in which evaluation occurrs, which assigns values to variables.
data Env = Env {
    lookupField :: FieldName -> LookupResult,
    subFields   :: FieldName -> Maybe Env
}

-- | An empty environment in which no variables are defined.
emptyEnv :: Env
emptyEnv = Env {
    lookupField = const FieldDoesNotExist,
    subFields   = const Nothing
}

-- | The looked up value of a certain field, or an error in case the expected type does not match the actual type of
--   an expression.
data LookupResult
    = Success ExpValue
    | FieldDoesNotExist
    | NoExpressionType
    -- ^ The field has a type of which values can not be used in an expression.

-- | Computes an expression, given an environment.
evaluate :: Env -> Expression -> Either InvalidExpression ExpValue
evaluate env exp =
    case exp of
        Constant x           -> return x
        Var name             -> getField env name
        Listed exps          -> liftM EList $ sequence $ map (evaluate env) exps
        FieldAccess f1 f2 fs -> subField env $ f1:f2:fs
        Operator lhs op rhs  -> do
            a <- evaluate env lhs
            b <- evaluate env rhs
            either (Left . ($ exp)) Right $ evalOp op a b
        Conditional cond yes no -> do
            cond' <- evaluate env cond
            -- Both branches are intentionally evaluated.
            yes' <- evaluate env yes
            no'  <- evaluate env no
            case cond' of
                EBool True  -> return yes'
                EBool False -> return no'
                _           -> Left $ TypeError exp

 where 
    evalOp op l r =
        case (l, op, r) of
            (EBool a, BAnd, EBool b) -> return $ EBool $ a && b
            (EBool a, BOr,  EBool b) -> return $ EBool $ a || b
            (EBool a, BXor, EBool b) -> return $ EBool $ not a == b
            (a, BEq, b)              -> return $ EBool $ a == b
            (EInt a,    BLess, EInt b)    -> return $ EBool $ a < b
            (EBinary a, BLess, EBinary b) -> return $ EBool $ comparing B.toList a b == LT
            (EText a,   BLess, EText b)   -> return $ EBool $ a < b
            (EList [],  BLess, EList [])  -> return $ EBool False
            (EList (a:as), BLess, EList (b:bs)) | a == b -> evalOp BLess (EList as) (EList bs)
                                                | otherwise -> evalOp BLess a b
            (EList a, BConcat, EList b)     -> return $ EList $ a ++ b
            (EText a, BConcat, EText b)     -> return $ EText $ a ++ b
            (EBinary a, BConcat, EBinary b) -> return $ EBinary $ B.append a b
            (EInt a, BPlus, EInt b)  -> return $ EInt $ a + b
            (EInt a, BMinus, EInt b) -> return $ EInt $ a - b
            (EInt a, BMul, EInt b)   -> return $ EInt $ a * b
            (EInt a, BDiv, EInt 0)   -> Left DivisionByZero
            (EInt a, BDiv, EInt b)   -> return $ EInt $ a `quot` b
            (EInt a, BMod, EInt 0)   -> Left DivisionByZero
            (EInt a, BMod, EInt b)   -> return $ EInt $ a `rem` b
            (EInt a, BPow, EInt b)   -> return $ EInt $ a ^ b
            _ -> Left TypeError

    getField env' name =
        case lookupField env' name of 
                Success x         -> Right x
                FieldDoesNotExist -> Left $ UnknownField name
                NoExpressionType  -> Left $ UnusableField name

    subField env' [f] = getField env' f
    subField env' (f:fs) =
        case subFields env' f of
            Just subEnv -> subField subEnv fs
            Nothing     -> Left $ UnknownField f

data InvalidExpression
    = TypeError Expression
    | UnknownField FieldName
    | UnusableField FieldName
    | DivisionByZero Expression
    | NotAValueExpression
    deriving (Show)

-- | Combine two arguments lists. When both lists contain an argument with the same parameter, the one from the first
--   list is prioritized.
addArgs :: Arguments' ty co -> Arguments' ty co -> Arguments' ty co
addArgs (Arguments a1) (Arguments a2) = Arguments $ unionBy (\(k1,_) (k2, _) -> k1 == k2) a1 a2