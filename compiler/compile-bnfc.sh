#!/bin/bash

cd src
bnfc -d -p Language.APSL.AMSL Language/APSL/AMSL/Grammar.cf
alex Language/APSL/AMSL/Grammar/Lex.x
happy Language/APSL/AMSL/Grammar/Par.y

bnfc -d -p Language.APSL.AISL Language/APSL/AISL/Grammar.cf 
alex Language/APSL/AISL/Grammar/Lex.x
happy Language/APSL/AISL/Grammar/Par.y